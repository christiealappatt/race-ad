set(RACE_INCLUDE_DIRS /home/hpc/unrz/unrz002h/race-ad/lib/include ${HWLOC_INCLUDE_DIR})

if (ON STREQUAL "ON")
    set(RACE_LIBRARIES /home/hpc/unrz/unrz002h/race-ad/lib/lib/RACE/libRACE.so -lpthread)
else()
    set(RACE_LIBRARIES /home/hpc/unrz/unrz002h/race-ad/lib/lib/RACE/libRACE.a -lpthread)
endif()
set(RACE_LIBDIR /home/hpc/unrz/unrz002h/race-ad/lib/lib)

set(RACE_C_FLAGS "${RACE_C_FLAGS}")
set(RACE_CXX_FLAGS "${RACE_CXX_FLAGS} -std=c++11")

set(RACE_CC )
set(RACE_CXX )

set(RACE_VERSION 0.4.0)

# set default paths for libraries
set(RACE_LIBRARIES ${RACE_LIBRARIES} ${HWLOC_LIBRARIES})

if (1)
    set(RACE_C_FLAGS "${RACE_C_FLAGS} -qopenmp")
    set(RACE_CXX_FLAGS "${RACE_CXX_FLAGS} -qopenmp")
endif()
