#include <stdio.h>
#include <omp.h>
#include "mmio.h"
#include "time.h"

#ifdef LIKWID_MEASURE
#include <likwid.h>
#endif
#include "parse.h"
#include "sparsemat.h"
#include "densemat.h"
#include "kernels.h"
#include "timer.h"


int main(const int argc, char * argv[])
{

    parser param;
    if(!param.parse_arg(argc, argv))
    {
        printf("Error in reading parameters\n");
    }
    sparsemat* mat = new sparsemat;

    printf("Reading matrix file\n");
    if(!mat->readFile(param.mat_file))
    {
        printf("Error in reading sparse matrix file\n");
    }
    printf("Coloring matrix\n");

    mat->colorAndPermute(TWO, param.cores, param.smt, param.pin);

    printf("Finished coloring\n\n");

    int NROWS = mat->nrows;

    densemat *x, *x_soln, *b;
    x=new densemat(NROWS);
    x_soln=new densemat(NROWS);
    b=new densemat(NROWS);

    x->setVal(0);
    x_soln->setVal(1);
    b->setVal(0);

    //find b=A*x
    spmv(b,mat,x_soln,1);

    int iter = 0;

    sleep(1);

    double err=0;
    INIT_TIMER(kernel);
    START_TIMER(kernel);
    do
    {
        symm_kacz(b,mat,x,1);
       //find error
        err = 0;
#pragma omp parallel for schedule(static) reduction(+:err)
        for(int i=0; i<x->nrows; ++i)
        {
            double dev = (x_soln->val[i]-x->val[i]);
            err+=(dev*dev);
        }
        ++iter;
    } while(sqrt(err/static_cast<double>(b->nrows))>param.tol && iter<param.iter);

    STOP_TIMER(kernel);
    double time = GET_TIMER(kernel);

    printf("Iterations = %d, curr_error = %.8e\n", iter, sqrt(err/static_cast<double>(b->nrows)));
    double perf = (8.0*iter*(mat->nnz*1.e-9))/time;
    printf("Perf = %f\n", perf);

    delete mat;
    delete x;
    delete x_soln;
    delete b;
}
